package com.reto.jwt.util;

import java.util.List;

import com.reto.jwt.dto.CambiosBcp;

public interface GeneradorCambioService {

	public CambiosBcp calcularCambioBcp(CambiosBcp cambio);
	
	public  List<CambiosBcp> getCambiosMonedas(String cambiosMonedaOrigen);

}

package com.reto.jwt.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reto.jwt.dao.CambiosDao;
import com.reto.jwt.dto.CambiosBcp;

@Service
public class GenerarCambioServiceImp implements GeneradorCambioService {
	
	public static String TODOS = "TODOS";

	public static String SOL = "SOL";
	public static String DOL = "DOL";
	public static String EUR = "EUR";
	

	public static double SOL_PRECIOCAMBIO_DOL = 0.29;
	public static double SOL_PRECIOCAMBIO_EUR = 0.25;
	public static double DOL_PRECIOCAMBIO_SOL = 3.35;
	public static double DOL_PRECIOCAMBIO_EUR = 0.89;
	public static double EUR_PRECIOCAMBIO_SOL = 3.93;
	public static double EUR_PRECIOCAMBIO_DOL = 1.12;
	
	
	@Autowired
	private CambiosDao cambiosDao;
	 
	@Override
	public CambiosBcp calcularCambioBcp(CambiosBcp cambio) {
		
		double cambioCliente = 0.0;
		
		if(cambio.getMonedaOrigen().equals(SOL)) {
			if(cambio.getMonedaDestino().equals(DOL)) {
				
				cambio.setTipoCambio(SOL_PRECIOCAMBIO_DOL);
				cambioCliente = cambio.getMonto()/cambio.getTipoCambio();
				
			}else if(cambio.getMonedaDestino().equals(EUR)) {
				
				cambio.setTipoCambio(SOL_PRECIOCAMBIO_EUR);
				cambioCliente = cambio.getMonto()*cambio.getTipoCambio();
				
			}
		}else if(cambio.getMonedaOrigen().equals(DOL)) {
			if(cambio.getMonedaDestino().equals(SOL)) {
				
				cambio.setTipoCambio(DOL_PRECIOCAMBIO_SOL);
				cambioCliente = cambio.getMonto()*cambio.getTipoCambio();
				
			}else if(cambio.getMonedaDestino().equals(EUR)) {
				
				cambio.setTipoCambio(DOL_PRECIOCAMBIO_EUR);
				cambioCliente = cambio.getMonto()*cambio.getTipoCambio();
				
			}
		}else if(cambio.getMonedaOrigen().equals(EUR)) {
			if(cambio.getMonedaDestino().equals(SOL)) {
				
				cambio.setTipoCambio(EUR_PRECIOCAMBIO_SOL);
				cambioCliente = cambio.getMonto()*cambio.getTipoCambio();
				
			}else if(cambio.getMonedaDestino().equals(DOL)) {
				
				cambio.setTipoCambio(EUR_PRECIOCAMBIO_DOL);
				cambioCliente = cambio.getMonto()*cambio.getTipoCambio();
				
			}
		}
		
        BigDecimal cambioGenerado = new BigDecimal(cambioCliente).setScale(2, RoundingMode.HALF_UP);
		cambio.setMontoCambio(cambioGenerado.doubleValue());
		
		return cambiosDao.registerCambiosBcp(cambio);
	}
	
	
	public  List<CambiosBcp> getCambiosMonedas(String cambiosMonedaOrigen){
		List<CambiosBcp> lstCambiosBcp = null;
    	if(cambiosMonedaOrigen != null) {
    		if(cambiosMonedaOrigen.equals(TODOS)) {
        		lstCambiosBcp = cambiosDao.findAll();
        	}else {
        		lstCambiosBcp = cambiosDao.getCambiosMonedas(cambiosMonedaOrigen);
        	}
    	}
    	return lstCambiosBcp;
	}
	
}

package com.reto.jwt.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CambiosBcp {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private double monto;
	private String monedaOrigen;
	private String monedaDestino;
	private double montoCambio;
	private double tipoCambio;
	
	public CambiosBcp() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public String getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public String getMonedaDestino() {
		return monedaDestino;
	}
	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}
	public double getMontoCambio() {
		return montoCambio;
	}
	public void setMontoCambio(double montoCambio) {
		this.montoCambio = montoCambio;
	}
	public double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
}

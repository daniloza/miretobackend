package com.reto.jwt.dto;

import java.util.List;

 
public class CambiosBcpResponse {
	
	public List<CambiosBcp> items;

	public List<CambiosBcp> getItems() {
		return items;
	}

	public void setItems(List<CambiosBcp> items) {
		this.items = items;
	}

	 
	
}

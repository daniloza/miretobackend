package com.reto.jwt.dto;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;
	
	private String user;
	private String token;
	
	public User()
	{
		
	}

	public User(String username, String token) {
		this.setUser(username);
		this.setToken(token);
	}
 
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
 
}
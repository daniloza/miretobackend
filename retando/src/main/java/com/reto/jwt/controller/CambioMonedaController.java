package com.reto.jwt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reto.jwt.dto.CambiosBcp;
import com.reto.jwt.dto.CambiosBcpResponse;
import com.reto.jwt.util.GeneradorCambioService;

@RestController
@RequestMapping("/v2")
@CrossOrigin(origins = "http://localhost")
public class CambioMonedaController {
	 
	@Autowired
    private GeneradorCambioService generadorCambioService;
	    
	@PostMapping("/hacerCambioBcp")
	public CambiosBcp hacerCambio(@RequestBody CambiosBcp cambio) {
		cambio = generadorCambioService.calcularCambioBcp(cambio);
		return cambio;
	}
	
    @GetMapping("/getCambiosMonedas/{cambiosMonedaOrigen}")
    public  CambiosBcpResponse getCambiosMonedas(@PathVariable String cambiosMonedaOrigen) {
    	CambiosBcpResponse response = new CambiosBcpResponse();
    	response.setItems(generadorCambioService.getCambiosMonedas(cambiosMonedaOrigen));
    	return response;
    }
	
}
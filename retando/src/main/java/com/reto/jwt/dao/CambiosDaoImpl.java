package com.reto.jwt.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.reto.jwt.dto.CambiosBcp;
import com.reto.jwt.repository.CambioRepository;

@Repository
public class CambiosDaoImpl implements CambiosDao{
	
	@Autowired
    private CambioRepository repository;
	
	@Override
	public  List<CambiosBcp> getCambiosMonedas(String cambiosMonedaOrigen){
	    return repository.findByMonedaDestino(cambiosMonedaOrigen);
	}
	
	@Override
	public  List<CambiosBcp> findAll(){
    	return repository.findAll();
	}
	
	@Override
	public  CambiosBcp registerCambiosBcp(CambiosBcp cambio) {
		return repository.save(cambio);
	} 
	
}

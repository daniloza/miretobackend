package com.reto.jwt.dao;

import java.util.List;

import com.reto.jwt.dto.CambiosBcp;

public interface CambiosDao {

	public  CambiosBcp registerCambiosBcp(CambiosBcp cambio);
	
	public  List<CambiosBcp> findAll();
	
	public  List<CambiosBcp> getCambiosMonedas(String cambiosMonedaOrigen);
	
}

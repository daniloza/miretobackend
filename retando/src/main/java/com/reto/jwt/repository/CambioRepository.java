package com.reto.jwt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.reto.jwt.dto.CambiosBcp;

public interface CambioRepository extends JpaRepository<CambiosBcp,Integer> {

	List<CambiosBcp> findByMonedaDestino(String monedaDestino);
	
}
